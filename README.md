# VentilAid
VentilAid design and source code files have been split into several independent repositories. 
Please use following links:
1. Mechanics: (Beta version) https://gitlab.com/Urbicum/ventilaidmechanics
1. Electronics: (Beta version) https://gitlab.com/Urbicum/ventilaidcpapcontroldevicehardware
1. Core software: (Alpha version) https://gitlab.com/Urbicum/ventilaidcpapcontroldevice/
1. User Interface: (Beta version) https://gitlab.com/Urbicum/ventilaidcpapcontrolpanel
1. Alternative User Interface: (Beta version) https://gitlab.com/Urbicum/ventilaidhmisoftware


Unless otherwise stated, all information distributed here, comes under terms and conditions described in file “license” included in the repository.

All repositories can be used independently, to allow users to accommodate for their needs. They are aimed at providing general, universal solutions, for sub-blocks of various breathing-related hardware. You are strongly advised to start from replicating simple CPAP functionality, using one pressure sensor in U12/U13 position of the main board. Single sensor is also enough to replicate BiPAP functionality without volume control. For more sophisticated modes of operation, additional sensors are needed.

Old versions (v1 and v2) are considered obsolete and potentialy dangerous, and should not be used. They are kept in the repository just for the reference.
