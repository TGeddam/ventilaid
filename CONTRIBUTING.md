English Version Below.

LICENCJA DLA KONTRYBUTORÓW

§1
Definicje
    1. „Licencja” oznacza niniejszą Licencję.
    2. „Licencja pierwotna” oznacza umowę licencyjną, na której udostępnione zostało Urządzenie i Dokumentacja przez Ventilaid Sp. z o.o.. Interpretacja niniejszej Licencji musi odbywać się w zgodzie i z zachowaniem odwołań definicyjnych do tych użytych w Licencji pierwotnej. Licencja pierwotna dostępna jest pod adresem: https://gitlab.com/Urbicum/ventilaid/-/blob/master/LICENSE
    3. „Kontrybutor”  - oznacza osobę, która dokonała modyfikacji lub jakiejkolwiek zmiany w Urządzeniu lub Dokumentacji i udostępnia wprowadzone modyfikacje.
    4. „Modyfikacja” oznacza jakąkolwiek zmianę techniczną bądź inną wprowadzoną do Dokumentacji i Urządzenia udostępnionego przez VentilAid Sp. z o.o.. 

§2
Zakres licencji
    1. Z zastrzeżeniem warunków niniejszej Umowy, niniejszym udzielasz Ventilaid Sp. z o.o. i wszelkim innym podmiotom – osobom fizycznym i prawnym,  wieczystą, ogólnoświatową, niewyłączną, bezpłatną, nieodpłatną, nieodwołalną licencję na kopiowanie, modyfikowanie, przygotowywanie dzieł pochodnych (w tym wcielanie modyfikacji do pierwotnej Dokumentacji), publiczne prezentowanie, publiczne wykonywanie, udzielanie podlicencji i rozpowszechnianie Twojego zmodyfikowanego Urządzenia lub zmodyfikowanej Dokumentacji. 
    2. Niniejsza Licencja dotyczy swoim zakresem jedynie Modyfikacji i w żaden sposób nie wpływa na udostępnianie Urządzenia i Dokumentacji na Licencji pierwotnej.  



§3
Upoważnienie do udzielania licencji

    1. Oświadczasz, że jesteś prawnie upoważniony do udzielenia powyższej licencji. Jeśli Twój pracodawca ma prawa do własności intelektualnej, którą tworzysz, która obejmuje Twoje modyfikacje, oświadczasz, że otrzymałeś pozwolenie na udzielenie licencji w imieniu tego pracodawcy.
    2. Oświadczasz, że każdy Twoja modyfikacja jest Twoim oryginalnym dziełem. Oświadczasz, że przesłane przez Ciebie materiały zawierają pełne szczegóły wszelkich licencji stron trzecich lub innych ograniczeń (w tym między innymi powiązanych patentów i znaków towarowych), o których jesteś osobiście świadomy i które są powiązane z dowolną częścią Twoich modyfikacji.
    3. Jeśli jakikolwiek podmiot wszczyna spór patentowy przeciwko Tobie lub jakiemukolwiek innemu podmiotowi (w tym roszczenie wzajemne lub roszczenie wzajemne w procesie sądowym), twierdząc, że Twoja modyfikacja stanowi bezpośrednie lub współdziałające naruszenie patentu, wówczas wszelkie udzielone licencje patentowe dla tego podmiotu na mocy niniejszej Umowy wygasają z dniem wniesienia takiego sporu.


§4
Ograniczenie odpowiedzialności

    1. Nie oczekuje się, że będziesz wspierać swój wkład, z wyjątkiem zakresu, w jakim chcesz udzielić wsparcia. Możesz udzielać pomocy bezpłatnie, za opłatą lub wcale. O ile nie jest to wymagane przez obowiązujące prawo lub nie zostało to uzgodnione na piśmie, przekazujesz swój wkład na zasadach „taki, jaki jest”, bez jakichkolwiek gwarancji lub warunków, wyraźnych lub dorozumianych.
    2. Udzielając niniejszej Licencji nie ponosisz odpowiedzialności za szkody bezpośrednie, pośrednie, szczególne, przypadkowe, wynikowe, przypadkowe, karne lub inne o jakimkolwiek charakterze, w tym między innymi za zakup towarów lub usług zastępczych, utratę danych lub zysków, lub przerw w działalności, niezależnie od tego, jak są spowodowane i na podstawie jakiejkolwiek umowy, gwarancji, czynu niedozwolonego (w tym zaniedbania), odpowiedzialności za produkt lub w inny sposób doszło do ich powstania w związku z udostępnioną zmodyfikowaną Dokumentacją lub Urządzeniem. 
    4. Zobowiązujesz się powiadomić VentilAid Sp. z o.o.  o wszelkich faktach lub okolicznościach, które mogą mieć istotny prawnie lub technicznie wpływ na korzystanie z Twojej modyfikacji, w szczególności jeżeli korzystanie naruszałoby prawa osób trzecich. 
§5
Inne postanowienia
    1. W przypadku nieważności któregokolwiek z zapisów Licencji, Licencja obowiązuje z jego wyłączaniem.
    2. W sprawach nieuregulowanych w niniejszej umowie stosuje się przepisy ustawy Prawo własności przemysłowej oraz kodeksu cywilnego.






§1
Definitions

    1. "License" refers to this License.

    2. "Primary License" refers to the license agreement under which the Device and Documentation were made available by Ventilaid Sp. z o.o .. This License must be interpreted in accordance with and with reference to the definitions used in the Original License. The primary license is available at: https://gitlab.com/sbacher/ventilaid/-/blob/master/LICENSE

    3. "Contributor" refers to a person who has made a modification or any change to the Equipment or Documentation and makes the modifications available.

    4. "Modification" refers to any technical or other change made to the Documentation and the Device that was made available by VentilAid Sp. z o.o.



§2
Scope of the license

    1. Subject to the terms and conditions of this Agreement, you hereby grant Ventilaid Sp. z o.o. and all other entities - individuals and legal entities, a perpetual, worldwide, free, non-exclusive, royalty-free, irrevocable license to copy, modify, prepare derivative work (including incorporating modifications into the original Documentation), publicly display, publicly perform, sublicense, and distribute your modified Device or modified Documentation.
 
    2. By its terms, this License applies only to Modifications, and in no way affects the provision and release of the Equipment and Documentation under the Original License.

§3
Authorization to grant a license

    1. In the event when your employer has rights to intellectual property that you create, as well as intellectual property that includes any modifications you make, you hereby declare to have been authorized to grant such licenses on behalf of the said employer.

    2. You certify that each of your modifications is your original work. You warrant that the materials you submit contain the full details of any third-party licenses or other restrictions (including but not limited to associated patents and trademarks) of which you are personally aware and which are associated with any part of your modifications.

    3. In the event that any entity initiates patent litigation against you or any other entity (including a counterclaim or counterclaim in a lawsuit) claiming that your modification constitutes a direct or contributory patent infringement, then any patent licenses granted to that entity under this Agreement shall terminate as of the date of such litigation.


§4
Limitation of Liability

    1. You are not expected to support your contribution, as such the extent to which you wish to contribute is at your discretion. You may provide assistance for free, for a fee, or not at all. Unless required by applicable law or agreed to in writing, you provide your contribution "as is" and without warranties or conditions of any kind, explicit or implied.

    2. By granting this License, you shall not be liable for direct, indirect, special, incidental, consequential, incidental, punitive or other damages of any nature, including but not limited to the purchase of replacement goods or services, loss of data or profits, or business interruption, regardless of how they are caused and under any contract, warranty, tort (including negligence), product liability or otherwise arose in connection with the modified Documentation or Device provided.

    3. You are obliged to notify VentilAid Sp. z o.o. of any facts or circumstances that may have a legally or technically significant effect on the use of your modification, in particular if the use would infringe the rights of third parties.



§5
Other provisions

    1. In the event that any of the provisions of the License were invalid, the License shall be valid with the exclusion thereof.

    2. In matters not covered by this agreement, the provisions of the Industrial Property Law and the Civil Code shall apply.

